package fr.mainEtMere.design;

import Interfaces.AttaqueFusil;
import Interfaces.Courrir;
import Interfaces.Deplacer;
import Interfaces.EspritCombatif;
import Interfaces.Soin;

public class Guerier extends Personnage{

	public Guerier() {
		this.espritCombatif = new AttaqueFusil();
		this.deplacer = new Courrir();
	}

	public Guerier(EspritCombatif espritCombatif, Soin soin, Deplacer deplacer) {
		super(espritCombatif, soin, deplacer);
		// TODO Auto-generated constructor stub
	}
	
}
