package fr.mainEtMere.design;

import Interfaces.Deplacer;
import Interfaces.EspritCombatif;
import Interfaces.Marcher;
import Interfaces.Pacifique;
import Interfaces.PasPremierSoin;
import Interfaces.Soin;

public abstract class Personnage {
// Instances de comportement par defaut
	protected EspritCombatif espritCombatif = new Pacifique();
	protected Soin soin = new PasPremierSoin();
	protected Deplacer deplacer = new Marcher();

	public Personnage() {
	};

	public Personnage(EspritCombatif espritCombatif, Soin soin, Deplacer deplacer) {
		this.espritCombatif = espritCombatif;
		this.soin = soin;
		this.deplacer = deplacer;
	}

	public EspritCombatif getEspritCombatif() {
		return espritCombatif;
	}

	public Soin getSoin() {
		return soin;
	}

	public Deplacer getDeplacer() {
		return deplacer;
	}

	public void setEspritCombatif(EspritCombatif espritCombatif) {
		this.espritCombatif = espritCombatif;
	}

	public void setSoin(Soin soin) {
		this.soin = soin;
	}

	public void setDeplacer(Deplacer deplacer) {
		this.deplacer = deplacer;
	}

	// Utilisation de manière polymorphe
	public void combattre() {
		espritCombatif.combattre();
	}

// Utilisation de manière polymorphe
	public void soins() {
		soin.soigner();
	}

// Utilisation de manière polymorphe
	public void deplacement() {
		deplacer.deplacer();
	}
}
