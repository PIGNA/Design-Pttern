package fr.mainEtMere.design;

import Interfaces.Ambulance;
import Interfaces.Deplacer;
import Interfaces.EspritCombatif;
import Interfaces.Soin;
import Interfaces.TrousseMedecin;

public class Medecin extends Personnage{

	public Medecin() {
		this.soin = new TrousseMedecin();
		this.deplacer = new Ambulance();
	}

	public Medecin(EspritCombatif espritCombatif, Soin soin, Deplacer deplacer) {
		super(espritCombatif, soin, deplacer);
		// TODO Auto-generated constructor stub
	}
}
