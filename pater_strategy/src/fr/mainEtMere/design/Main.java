package fr.mainEtMere.design;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Personnage[] pers = { new Guerier(), new Medecin(), new Civile() };

		for (Personnage obj : pers) {
			System.out.println(" ");
			obj.deplacement();
		    obj.soins();
		    obj.combattre();}
	}

}
