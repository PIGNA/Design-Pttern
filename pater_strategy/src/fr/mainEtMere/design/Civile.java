package fr.mainEtMere.design;

import Interfaces.Deplacer;
import Interfaces.EspritCombatif;
import Interfaces.PremierSoin;
import Interfaces.Soin;

public class Civile extends Personnage {

	public Civile() {
		this.soin = new PremierSoin();
	}

	public Civile(EspritCombatif espritCombatif, Soin soin, Deplacer deplacer) {
		super(espritCombatif, soin, deplacer);
		// TODO Auto-generated constructor stub
	}

}
